#ifndef CINISERIALIZATION_H
#define CINISERIALIZATION_H

#include "Serialization/Serialization.h"
#include "cfileini.h"

class CIniSerialization : public ISerealization
{
	CFileIni *_ini;
public:
	CIniSerialization(const std::string &file);
	virtual ~CIniSerialization();

	virtual void sync();

protected:
	virtual void processWrite( std::type_index in_type, std::string in_var, void *in_ptr );
	virtual void processRead( std::type_index in_type, std::string in_var, void *out_ptr );

private:
	void replace(std::string &str, const std::string &old_str, const std::string &new_str);
};

#endif // CINISERIALIZATION_H
