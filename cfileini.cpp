#include "cfileini.h"
#include "cfileinfo.h"
#include <regex>

CFileIni::CFileIni( const std::string &fileName ) : CFileText( fileName ) {
	parse();
}

CFileIni::CFileIni( CFileInfo file ) : CFileText( file ) {
	parse();
}

CFileIni::~CFileIni() {
	sync();
}

bool CFileIni::sync() {
	const std::regex re( R"(\s+.*)" );
	std::cmatch		 cmatch;

	CFileText::clear();
	for ( auto &sections : ini ) {
		if ( sections.first.size() ) CFileText::add( "[" + sections.first + "]" );
		for ( auto &values : sections.second ) {
			for ( auto &str : values.second ) {
				if ( std::regex_match( str.c_str(), cmatch, re ) ) str = "\"" + str + "\"";
				for ( size_t i = 0; i < str.length(); ++i ) {
					if ( str[i] == '\\' ) {
						if ( str[i + 1] == 'n' )
							str.replace( i, 2, "\n" );
						else if ( str[i + 1] == 't' )
							str.replace( i, 2, "\t" );
						else if ( str[i + 1] == 'a' )
							str.replace( i, 2, "\a" );
						else if ( str[i + 1] == 'b' )
							str.replace( i, 2, "\b" );
						else if ( str[i + 1] == 'f' )
							str.replace( i, 2, "\f" );
						else if ( str[i + 1] == 'r' )
							str.replace( i, 2, "\r" );
						else if ( str[i + 1] == 'v' )
							str.replace( i, 2, "\v" );
						else if ( str[i + 1] == '\\' )
							str.replace( i, 2, "\\" );
					}
				}
				CFileText::add( values.first + "=" + str );
			}
		}
	}

	return CFileText::sync();
}

void CFileIni::clear() {
	CFileText::clear();
	ini.clear();
}

void CFileIni::restore() {
	CFileText::restore();
	clear();
	parse();
}

std::deque< std::string > &CFileIni::at( std::string key ) {
	const std::regex re( R"(([^\/]*)\/(.*))" );
	std::cmatch		 cmatch;

	if ( key.find( '/' ) == std::string::npos ) key.insert( 0, "/" );

	if ( std::regex_match( key.c_str(), cmatch, re ) ) return ini[cmatch[1].str()][cmatch[2].str()];

	throw std::runtime_error( "CFileIni: Key '" + key + "' was not valid" );
}

IniValues &CFileIni::readSection( const std::string &section ) {
	return ini[section];
}

std::string &CFileIni::value( const std::string &key ) {
	std::deque< std::string > &arr = at( key );
	if ( !arr.size() ) arr.push_back( "" );
	return arr.front();
}

std::deque< std::string > &CFileIni::array( const std::string &				 key,
											const std::deque< std::string > &defValue ) {
	if ( !at( key ).size() )
		for ( auto &value : defValue ) at( key ).push_back( value );
	return at( key );
}

bool CFileIni::isKeyExist( const std::string &key ) {
	bool ret = false;
	try {
		at( key ).front();
		ret = true;
	} catch ( std::exception & ) {
		ret = false;
	}
	return ret;
}

void CFileIni::removeSection( const std::string &section ) {
	ini.erase( section );
}

void CFileIni::removeKey( const std::string &key ) {
	at( key ).clear();
}

void CFileIni::removeOne( const std::string &key, const std::string &value ) {
	std::deque< std::string > &arr = at( key );
	for ( size_t i = 0; i < arr.size(); ++i ) {
		if ( arr.at( i ) == value ) {
			arr.erase( arr.begin() + i );
			return;
		}
	}
}

std::deque< std::string > &CFileIni::operator[]( const std::string &key ) {
	return at( key );
}

void CFileIni::parse() {
	const std::regex section( R"(\s*\[(.+)\].*)" );
	const std::regex key( R"(\s*(.+)\s*=\s*(.*))" );
	const std::regex strValue( R"(\"([^\"]*)\")" );
	const std::regex strValue2( R"(\'([^\']*)\')" );
	const std::regex strValue3( R"(\`([^\`]*)\`)" );
	std::cmatch		 cmatch;

	std::string activeSection = "";
	for ( auto str : file ) {
		if ( std::regex_match( str.c_str(), cmatch, section ) ) {
			activeSection = cmatch[1].str();
			continue;
		}
		if ( std::regex_match( str.c_str(), cmatch, key ) ) {
			std::string activeKey = cmatch[1].str();
			ini[activeSection][activeKey].push_back( cmatch[2].str() );

			if ( std::regex_match( str.c_str(), cmatch, strValue ) )
				ini[activeSection][activeKey].push_back( cmatch[1].str() );
			else if ( std::regex_match( str.c_str(), cmatch, strValue2 ) )
				ini[activeSection][activeKey].push_back( cmatch[1].str() );
			else if ( std::regex_match( str.c_str(), cmatch, strValue3 ) )
				ini[activeSection][activeKey].push_back( cmatch[1].str() );
		}
	}
}
