#include "cfilebin.h"
#include "cfileinfo.h"

CFileBin::CFileBin(const std::string &file) : CFileBase (file)
{
	reset();
	load();
	file_orig = this->file;
	file_sync = file_orig;
}

CFileBin::CFileBin(CFileInfo file) : CFileBase (file)
{
	CFileBin(file.file());
}

CFileBin::CFileBin(byte *address, size_t size) : CFileBase (address, size)
{
	reset();
	for (size_t i = 0; i < size; ++i)
		file.push_back(address[i]);
	file_orig = this->file;
	file_sync = file_orig;
}

CFileBin::~CFileBin()
{
	if (readOnly)
		return;
	if (!safeOnDelete)
		return;
	if (isModify())
		if (!save())
			throw std::runtime_error("Can't write file \"" + fileName + "\"");
}

bool CFileBin::sync()
{
	if (readOnly)
		return false;
	CFileBin *_file = new CFileBin(fileName);
	if (operator==(*_file))
		return false;

	if (file_sync != file){
		_file->file.clear();
		_file->file = file;
	} else {
		file.clear();
		file = _file->file;
	}
	file_sync = file;
	delete _file;
	return true;
}

void CFileBin::clear()
{
	file.clear();
	setOffset(0);
}

void CFileBin::restore()
{
	file = file_orig;
}

std::deque<byte>  &CFileBin::data()
{
	return file;
}

byte &CFileBin::at(const size_t &id)
{
	return file.at(id);
}

void CFileBin::reset()
{
	setOffset(0);
}

void CFileBin::setEndOffset()
{
	setOffset(size());
}

bool CFileBin::setOffset(size_t pos)
{
	if (pos > size())
		return false;
	offset = pos;
	return true;
}

size_t CFileBin::getOffset()
{
	return offset;
}

bool CFileBin::operator==(const std::deque<byte>  &rhs)
{
	return cmp(rhs);
}

bool CFileBin::operator==(const CFileBin &rhs)
{
	return cmp(rhs.file);
}

byte &CFileBin::operator[](const size_t &id)
{
	return at(id);
}

size_t CFileBin::size()
{
	return static_cast<size_t>(file.size());
}

bool CFileBin::cmp(const std::deque<byte>  &_file)
{
	if (file != _file)
		return false;
	return true;
}

bool CFileBin::isModify()
{
	return !cmp(file_orig);
}

bool CFileBin::load()
{
	std::ifstream f(fileName, std::ios::binary);
	if (!f.is_open())
		return false;
	while (!f.eof())
		file.push_back(static_cast<byte>(f.get()));
	file.pop_back();
	f.close();
	return true;
}

bool CFileBin::save()
{
	if (readOnly)
		return false;
	std::ofstream f(fileName, std::ios::binary);
	if (!f.is_open())
		return false;
	for (size_t i = 0; i < file.size(); i++)
		f << file[i];
	f.close();
	return true;
}
