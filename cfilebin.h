#ifndef CFILEBIN_H
#define CFILEBIN_H

#include "cfilebase.h"

namespace CFileBinRW{
	template<typename T> T read(const std::deque<byte>  &array, size_t &offset);
	template<> std::string read(const std::deque<byte>  &array, size_t &offset);
	template<> char* read(const std::deque<byte>  &array, size_t &offset);

	template<typename T> void write(std::deque<byte>  &array, size_t &offset, T value);
	template<> void write(std::deque<byte>  &array, size_t &offset, std::string value);
	template<> void write(std::deque<byte>  &array, size_t &offset, const char* value);
	template<> void write(std::deque<byte>  &array, size_t &offset, char* value);
	template<> void write(std::deque<byte>  &array, size_t &offset, std::deque<byte>  value);
}

class CFileBin : public CFileBase
{
public:
	explicit CFileBin( const std::string &file );
	explicit CFileBin( class CFileInfo file );
	explicit CFileBin( byte *address, size_t size );
	~CFileBin();

	virtual bool sync();
	virtual void clear();
	virtual void restore();

	virtual std::deque<byte>  &data();
	virtual byte &at(const size_t &id);
	virtual void reset();
	virtual void setEndOffset();
	virtual bool setOffset(size_t pos);
	virtual size_t getOffset();

	template<typename T> void write(T value){
		CFileBinRW::write<T>(file, offset, value);
	}

	template<typename T> T read(){
		return CFileBinRW::read<T>(file, offset);
	}

	bool operator==(const std::deque<byte>  &rhs);
	bool operator==(const CFileBin &rhs);
	byte &operator[](const size_t &id);

	size_t size();

protected:
	bool cmp(const std::deque<byte>  &_file);
	virtual bool isModify();

	std::deque<byte>  file;

private:
	bool load();
	bool save();

	std::deque<byte>  file_orig;
	std::deque<byte>  file_sync;
	size_t offset;
};

template<typename T> inline
T CFileBinRW::read(const std::deque<byte>  &array, size_t &offset){
	ByteDecomposition<T> bd;
	for(size_t i = 0; i < sizeof(T) && offset < array.size(); ++i, ++offset)
		bd.decomposition[i] = array[offset];
	return bd.value;
}
template<> inline
std::string CFileBinRW::read(const std::deque<byte>  &array, size_t &offset){
	std::string str;
	for(size_t i = 0; array.at(offset) != '\0' && offset < array.size(); ++i, ++offset)
		str += static_cast<char>(array.at(offset));
	offset++;
	return str;
}
template<> inline
char* CFileBinRW::read(const std::deque<byte>  &array, size_t &offset){
	return const_cast<char*>(read<std::string>(array, offset).c_str());
}

template<typename T> inline
void CFileBinRW::write(std::deque<byte>  &array, size_t &offset, T value){
	ByteDecomposition<T> bd;
	bd.value = value;
	for(size_t i = 0; i < sizeof(bd); ++i, ++offset){
		if (offset < array.size())
			array[offset] = bd.decomposition[i];
		else array.push_back(bd.decomposition[i]);
	}
}
template<> inline
void CFileBinRW::write(std::deque<byte>  &array, size_t &offset, std::string value){
	for(size_t i = 0; i < value.length(); ++i, ++offset){
		if (offset < array.size())
			array[offset] = static_cast<byte>(value[i]);
		else array.push_back(static_cast<byte>(value[i]));
	}
	if (offset++ < array.size())
		array[offset - 1] = '\0';
	else array.push_back('\0');
}
template<> inline
void CFileBinRW::write(std::deque<byte>  &array, size_t &offset, const char* value){
	write<std::string>(array, offset, value);
}
template<> inline
void CFileBinRW::write(std::deque<byte>  &array, size_t &offset, char* value){
	write<std::string>(array, offset, value);
}

template<> inline
void CFileBinRW::write(std::deque<byte>  &array, size_t &offset, std::deque<byte>  value){
	for(size_t i = 0; i < value.size(); ++i, ++offset){
		if (offset < array.size())
			array[offset] = value[i];
		else array.push_back(value[i]);
	}
}

#endif // CFILEBIN_H
