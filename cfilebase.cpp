#include "cfilebase.h"
#include "cfileinfo.h"

CFileBase::CFileBase(const std::string &file)
	: fileName(file)
{
	safeOnDelete = true;
}

CFileBase::CFileBase(CFileInfo file)
	: fileName(file.file())
{
}

CFileBase::CFileBase(void *address, size_t size)
{
	readOnly = true;
}

CFileBase::~CFileBase()
{
}

bool CFileBase::isSafeOnDelete()
{
	return safeOnDelete;
}

void CFileBase::toggleSafeOnDelete(bool as)
{
	safeOnDelete = as;
}

bool CFileBase::isReadOnly()
{
	return readOnly;
}

bool CFileBase::isExist()
{
	return CFileInfo::exist(fileName);
}
