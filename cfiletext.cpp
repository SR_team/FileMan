#include "cfiletext.h"
#include "cfileinfo.h"

CFileText::CFileText(const std::string &file) : CFileBase (file)
{
	load();
	file_orig = this->file;
	file_sync = file_orig;
}

CFileText::CFileText(CFileInfo file) : CFileBase (file)
{
	CFileText(file.file());
}

CFileText::CFileText(byte *address, size_t size) : CFileBase (address, size)
{
	std::string str;
	for (size_t i = 0; i < size; ++i){
		if (address[i] == '\n' ||
			(address[i] == '\n' && address[i + 1] == '\r') ||
			(address[i] == '\r' && address[i + 1] == '\n')){
			if (address[i + 1] == '\r' || address[i + 1] == '\n')
				++i;
			file.push_back(str);
			str.clear();
		} else str.push_back(address[i]);
	}
	if (!str.empty())
		file.push_back(str);
	file_orig = this->file;
	file_sync = file_orig;
}

CFileText::~CFileText()
{
	if (readOnly)
		return;
	if (!safeOnDelete)
		return;
	if (isModify())
		if (!save())
			throw std::runtime_error("Can't write file \"" + fileName + "\"");
}

bool CFileText::sync()
{
	if (readOnly)
		return false;
	CFileText *_file = new CFileText(fileName);
	if ( operator==( *_file ) ) {
		delete _file;
		return false;
	}

	if (file_sync != file){
		_file->file.clear();
		_file->file = file;
	} else {
		file.clear();
		file = _file->file;
	}
	file_sync = file;
	delete _file;
	return true;
}

void CFileText::clear()
{
	file.clear();
}

void CFileText::restore()
{
	file = file_orig;
}

std::deque<std::string> &CFileText::data()
{
	return file;
}

std::string &CFileText::at(const size_t &id)
{
	return file[id];
}

void CFileText::removeString(const size_t &id)
{
	for(size_t i = 0; i < file.size(); ++i){
		if (i == id){
			file.erase(file.begin() + i);
			return;
		}
	}
}

void CFileText::removeOne(const std::string &value)
{
	for(size_t i = 0; i < file.size(); ++i){
		if (file.at(i) == value){
			file.erase(file.begin() + i);
			return;
		}
	}
}

bool CFileText::insert(const size_t &pos, const std::string &str)
{
	if (pos >= count())
		return false;
	auto iter = file.begin();
	for (size_t i = 0;iter != file.end() && i < pos; ++iter);
	file.insert(iter, str);
	return true;
}

void CFileText::add(const std::string &str)
{
	file.push_back(str);
}

void CFileText::add(const char *str)
{
	file.push_back(str);
}

bool CFileText::cmp(const std::deque<std::string> &_file)
{
	if (file != _file)
		return false;
	return true;
}

bool CFileText::operator==(const std::deque<std::string> &rhs)
{
	return file == rhs;
}

bool CFileText::operator==(const CFileText &rhs)
{
	return cmp(rhs.file);
}

std::string &CFileText::operator[](const size_t &id)
{
	return at(id);
}

size_t CFileText::count()
{
	return file.size();
}

bool CFileText::isModify()
{
	return !cmp(file_orig);
}

bool CFileText::load()
{
	std::ifstream f((char*)fileName.c_str(), std::ios::binary);
	if (!f.is_open())
		return false;
	std::string tmp;
	while (std::getline(f, tmp)){
		file.push_back(tmp);
	}
	f.close();
	return true;
}

bool CFileText::save()
{
	if (readOnly)
		return false;
	std::ofstream f((char*)fileName.c_str(), std::ios::binary);
	if (!f.is_open())
		return false;
	for (size_t i = 0; i < file.size(); i++)
		f << file[i] << std::endl;
	f.close();
	return true;
}

