#ifndef CFILEINFO_H
#define CFILEINFO_H

#include "cfilebase.h"

class CFileInfo
{
public:
	CFileInfo( const std::string &file );
	virtual ~CFileInfo(){}

	virtual const std::string file();
	virtual const std::string path();
	virtual const std::string fullName();
	virtual const std::string name();
	virtual const std::string suffix();
	virtual const std::string ext();

	virtual bool isExist();
	virtual bool remove();
	virtual bool rename( const std::string &newName, bool nameWithSuffix = true );
	virtual CFileInfo copy( const std::string &cpName );
	virtual void move( const std::string &mvName );

	void operator=( const std::string &file );

	static bool exist( const std::string &file );
	static bool remove( const std::string &file );
	static bool rename( const std::string &file_old, const std::string &file_new );
	static void copy( const std::string &file, const std::string &file_copy );
	static void move( const std::string &file_old, const std::string &file_new );

protected:
	virtual void parse();

private:
	std::string _file;
	std::string _path;
	std::string _fullName;
	std::string _name;
	std::string _suffix;
	std::string _ext;
};

#endif // CFILEINFO_H
