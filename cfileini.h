#ifndef CINIFILE_H
#define CINIFILE_H

#include "cfiletext.h"

namespace CFileIniRW{
	template<typename T> T read(std::string &str);
	template<> std::string read(std::string &str);
	template<> char* read(std::string &str);

	template<typename T> void write(std::string &str, T value);
	template<> void write(std::string &str, std::string value);
	template<> void write(std::string &str, const char* value);

	template<typename T> void add(std::deque<std::string> &arr, T value);
	template<> void add(std::deque<std::string> &arr, std::string value);
	template<> void add(std::deque<std::string> &arr, const char* value);
}

class CFileIni : CFileText
{
public:
	explicit CFileIni(const std::string &fileName);
	explicit CFileIni(class CFileInfo file);
	~CFileIni();

	virtual bool sync();
	virtual void clear();
	virtual void restore();

	virtual std::deque<std::string> &at(std::string key);

	virtual IniValues &readSection(const std::string &section);

	virtual std::string &value(const std::string &key);
	virtual std::deque<std::string> &array(const std::string &key, const std::deque<std::string> &defValue = {});

	virtual bool isKeyExist(const std::string &key);

	virtual void removeSection(const std::string &section);
	virtual void removeKey(const std::string &key);
	virtual void removeOne(const std::string &key, const std::string &value);

	template<typename T>
	T init(const std::string &key, T defValue = T()){
		if (!isKeyExist(key) || !value(key).size()){
			write<T>(key, defValue);
			return defValue;
		}
		T ret;
		try {
			ret = read<T>(key);
		} catch (std::exception &) {
			ret = (T)0;
		}
		return ret;
	}
	template<typename T>
	T read(const std::string &key){
		return CFileIniRW::read<T>(value(key));
	}
	template<typename T>
	void write(const std::string &key, T value){
		CFileIniRW::write<T>(this->value(key), value);
	}
	template<typename T>
	void add(const std::string &key, T value){
		CFileIniRW::add(array(key), value);
	}

	std::deque<std::string> &operator[](const std::string &key);

protected:
	virtual void parse();
	IniSections ini;

private:
	virtual size_t count() {return 0;}
	virtual bool insert(const size_t &, const std::string &) {return false;}
	virtual std::deque<std::string> &data() {
		static std::deque<std::string> dummy;
		return dummy;
	}
};


template<typename T> inline
T CFileIniRW::read(std::string &str){
	T ret = 0;
	try{
		if (typeid (T) == typeid (char) ||
			typeid (T) == typeid (bool) ||
			typeid (T) == typeid (short) ||
			typeid (T) == typeid (int) ||
			typeid (T) == typeid (long) ||
			typeid (T) == typeid (long long) ||
			typeid (T) == typeid (unsigned long))
			return (T)std::stoll(str);
		else if (typeid (T) == typeid (unsigned char) ||
				 typeid (T) == typeid (unsigned short) ||
				 typeid (T) == typeid (unsigned) ||
				 typeid (T) == typeid (unsigned int) ||
				 typeid (T) == typeid (unsigned long) ||
				 typeid (T) == typeid (unsigned long long))
			return (T)std::stoull(str);
		else if (typeid (T) == typeid (float) ||
				 typeid (T) == typeid (double) ||
				 typeid (T) == typeid (long double))
			return (T)std::stold(str);
	}
	catch (std::exception e){ }
	return ret;
}
template<> inline
std::string CFileIniRW::read(std::string &str){
	return str;
}
template<> inline
char* CFileIniRW::read(std::string &str){
	return const_cast<char*>(str.c_str());
}

template<typename T> inline
void CFileIniRW::write(std::string &str, T value){
	str = std::to_string(value);
}
template<> inline
void CFileIniRW::write(std::string &str, std::string value){
	str = value;
}
template<> inline
void CFileIniRW::write(std::string &str, const char* value){
	str = value;
}

template<typename T> inline
void CFileIniRW::add(std::deque<std::string> &arr, T value){
	arr.push_back(std::to_string(value));
}
template<> inline
void CFileIniRW::add(std::deque<std::string> &arr, std::string value){
	arr.push_back(value);
}
template<> inline
void CFileIniRW::add(std::deque<std::string> &arr, const char* value){
	arr.push_back(value);
}
#endif // CINIFILE_H
