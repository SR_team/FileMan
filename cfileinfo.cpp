#include "cfileinfo.h"
#include "cfilebin.h"
#include <regex>

CFileInfo::CFileInfo(const std::string &file) : _file(file)
{
	parse();
}

const std::string CFileInfo::file()
{
	return _file;
}

const std::string CFileInfo::path()
{
	return _path;
}

const std::string CFileInfo::fullName()
{
	return _fullName;
}

const std::string CFileInfo::name()
{
	return _name;
}

const std::string CFileInfo::suffix()
{
	return _suffix;
}

const std::string CFileInfo::ext()
{
	return _ext;
}

bool CFileInfo::isExist()
{
	return exist(_file);
}

bool CFileInfo::remove()
{
	return remove(_file);
}

bool CFileInfo::rename(const std::string &newName, bool nameWithSuffix)
{
	std::string oldFile = _file;
	if (nameWithSuffix)
		_file = _path + newName;
	else _file = _path + newName + _suffix;
	parse();
	return rename(oldFile, _file);
}

CFileInfo CFileInfo::copy(const std::string &cpName)
{
	copy(_file, cpName);
	return CFileInfo(cpName);
}

void CFileInfo::move(const std::string &mvName)
{
	move(_file, mvName);
	_file = mvName;
	parse();
}

void CFileInfo::operator=(const std::string &file)
{
	_file = file;
	parse();
}

bool CFileInfo::exist(const std::string &file)
{
	std::ifstream f(file);
	return f.good();
}

bool CFileInfo::remove(const std::string &file)
{
	return std::remove(file.c_str()) == 0;
}

bool CFileInfo::rename(const std::string &file_old, const std::string &file_new)
{
	return std::rename(file_old.c_str(), file_new.c_str()) == 0;
}

void CFileInfo::copy(const std::string &file, const std::string &file_copy)
{
	CFileBin f(file);
	CFileBin fc(file_copy);

	if (fc.isExist())
		fc.clear();

	for (size_t i = 0; i < f.size(); ++i)
		fc.write<byte>(f.at(i));

	fc.sync();
}

void CFileInfo::move(const std::string &file_old, const std::string &file_new)
{
	copy(file_old, file_new);
	remove(file_old);
}

void CFileInfo::parse()
{
	const std::regex path(R"((.+\/)(.+))");
	const std::regex name(R"(([^\.]+)\.(.*))");
	const std::regex ext(R"(.+\.([^\.]+))");
	std::cmatch m;

#ifdef WIN32
	for (auto &ch : this->_file)
		if (ch == '\\')
			ch = '/';
#endif

	if (std::regex_match(_file.c_str(), m, path)){
		_path = m[1].str();
		_fullName = m[2].str();
		if (std::regex_match(m[2].str().c_str(), m, name)){
			_name = m[1].str();
			_suffix = m[2].str();
			if (std::regex_match(m[2].str().c_str(), m, ext))
				_ext = m[1].str();
			else _ext = _suffix;
		}
	}
	else _path = "./";
}
