#ifndef CFILETEXT_H
#define CFILETEXT_H

#include "cfilebase.h"

class CFileText : public CFileBase
{
public:
	explicit CFileText( const std::string &file );
	explicit CFileText( class CFileInfo file );
	explicit CFileText( byte *address, size_t size );
	~CFileText();

	virtual bool sync();
	virtual void clear();
	virtual void restore();
	virtual std::deque<std::string> &data();
	virtual std::string &at(const size_t &id);

	virtual void removeString(const size_t &id);
	virtual void removeOne(const std::string &value);

	bool operator==(const std::deque<std::string> &rhs);
	bool operator==(const CFileText &rhs);
	std::string &operator[](const size_t &id);

	virtual size_t count();
	virtual bool insert(const size_t &pos, const std::string &str);
	void add(const std::string &str);
	void add(const char* str);
	template<typename T> inline
	void add(T v){
		add(std::to_string(v));
	}

protected:
	bool cmp(const std::deque<std::string> &_file);
	virtual bool isModify();

	std::deque<std::string> file;

private:
	bool load();
	bool save();

	std::deque<std::string> file_orig;
	std::deque<std::string> file_sync;
};

#endif // CFILETEXT_H
