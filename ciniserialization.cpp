#include "ciniserialization.h"

CIniSerialization::CIniSerialization( const std::string &file ) {
	_ini = new CFileIni( file );
}

CIniSerialization::~CIniSerialization() {
	write();
	delete _ini;
}

void CIniSerialization::sync() {
	_ini->sync();
}

void CIniSerialization::processWrite( std::type_index in_type, std::string in_var, void *in_ptr ) {
	replace( in_var, "__", "/" );
	if ( in_type == typeid( int ) || in_type == typeid( bool ) )
		_ini->write<int>( in_var, *(int *)in_ptr );
	else if ( in_type == typeid( unsigned int ) )
		_ini->write<unsigned int>( in_var, *(unsigned int *)in_ptr );
	else if ( in_type == typeid( float ) )
		_ini->write<float>( in_var, *(float *)in_ptr );
	else if ( in_type == typeid( double ) )
		_ini->write<double>( in_var, *(double *)in_ptr );
	else if ( in_type == typeid( short ) )
		_ini->write<short>( in_var, *(short *)in_ptr );
	else if ( in_type == typeid( unsigned short ) )
		_ini->write<unsigned short>( in_var, *(unsigned short *)in_ptr );
	else if ( in_type == typeid( byte ) )
		_ini->write<byte>( in_var, *(byte *)in_ptr );
	else if ( in_type == typeid( char ) )
		_ini->write<char>( in_var, *(char *)in_ptr );
	else if ( in_type == typeid( const char * ) || in_type == typeid( char * ) || in_type == typeid( std::string ) )
		_ini->write<std::string>( in_var, *(std::string *)in_ptr );
	else
		throw in_type;
}

void CIniSerialization::processRead( std::type_index in_type, std::string in_var, void *out_ptr ) {
	replace( in_var, "__", "/" );
	if ( in_type == typeid( int ) || in_type == typeid( bool ) )
		*(int *)out_ptr = _ini->init<int>( in_var, *(int *)out_ptr );
	else if ( in_type == typeid( unsigned int ) )
		*(unsigned int *)out_ptr = _ini->init<unsigned int>( in_var, *(unsigned int *)out_ptr );
	else if ( in_type == typeid( float ) )
		*(float *)out_ptr = _ini->init<float>( in_var, *(float *)out_ptr );
	else if ( in_type == typeid( double ) )
		*(double *)out_ptr = _ini->init<double>( in_var, *(double *)out_ptr );
	else if ( in_type == typeid( short ) )
		*(short *)out_ptr = _ini->init<short>( in_var, *(short *)out_ptr );
	else if ( in_type == typeid( unsigned short ) )
		*(unsigned short *)out_ptr = _ini->init<unsigned short>( in_var, *(unsigned short *)out_ptr );
	else if ( in_type == typeid( byte ) )
		*(byte *)out_ptr = _ini->init<byte>( in_var, *(byte *)out_ptr );
	else if ( in_type == typeid( char ) )
		*(char *)out_ptr = _ini->init<char>( in_var, *(char *)out_ptr );
	else if ( in_type == typeid( const char * ) || in_type == typeid( char * ) || in_type == typeid( std::string ) )
		*(std::string *)out_ptr = _ini->init<std::string>( in_var, *(std::string *)out_ptr );
	else
		throw in_type;
}

void CIniSerialization::replace( std::string &str, const std::string &old_str, const std::string &new_str ) {
	size_t find_pos = 0;
	while ( true ) {
		size_t pos = str.find( old_str, find_pos );
		if ( pos == str.npos ) break;
		str.replace( pos, old_str.length(), new_str );
		find_pos = pos + new_str.length();
	}
}
