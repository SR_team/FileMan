#ifndef CFILEBASE_H
#define CFILEBASE_H

#include <typeinfo>
#include <fstream>
#include <string>
#include <deque>
#include <map>

typedef unsigned char byte;
typedef unsigned int uint;
typedef std::map<std::string, std::deque<std::string>> IniValues;
typedef std::map<std::string, IniValues> IniSections;

template<typename T>
union ByteDecomposition{
	T value;
	byte decomposition[sizeof(T)];
};

class CFileBase
{
public:
	explicit CFileBase( const std::string &file );
	explicit CFileBase( class CFileInfo file );
	explicit CFileBase( void *address, size_t size );
	virtual ~CFileBase();

	virtual bool sync() = 0;
	virtual void clear() = 0;
	virtual void restore() = 0;
	
	virtual bool isSafeOnDelete();
	virtual void toggleSafeOnDelete(bool as);

	virtual bool isReadOnly();

	virtual bool isExist();

protected:
	virtual bool isModify() = 0;

	std::string fileName;
	bool safeOnDelete;
	bool readOnly;
};

#endif // CFILEBASE_H
