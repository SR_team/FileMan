#include <iostream>
#include "../cfilebin.h"
#include "../cfiletext.h"
#include "../cfileini.h"
#include "../cfileinfo.h"
#include "../ciniserialization.h"

using namespace std;

class SerializationTest : public CIniSerialization{
public:
	SerializationTest(const std::string &file)
		: CIniSerialization(file){}
	
	/* The ini file:
	 *
	 * floating=9.800000
	 * integer=7
	 * [section]
	 * string=Some string
	*/
	SERIALIZE_EXT(int, integer, 7);
	SERIALIZE_EXT(float, floating, 9.8);
	SERIALIZE_EXT(std::string, section__string, "Some string");
};

int main()
{
	CFileBin *fb = new CFileBin("./test.bin");
	fb->setEndOffset();
	fb->write<string>("Hello world");
	fb->write<float>(3.14f);
	fb->write<short>(456);
	fb->write<short>(-7);
	fb->write<byte>(0x90);
	//fb->reset();
	delete fb;
	fb = new CFileBin("./test.bin");
	cout << fb->size() << endl;
	cout << fb->read<string>() << endl;
	cout << fb->read<float>() << endl;
	cout << fb->read<short>() << endl;
	cout << fb->read<short>() << endl;
	cout << (int)fb->read<byte>() << endl;
	delete fb;

	CFileText *ft = new CFileText("./test.txt");
	ft->add("Hello world");
	ft->add(3.14);
	ft->add(456);
	ft->add(-7);
	cout << ft->at(0) << endl;
	cout << ft->at(1) << endl;
	cout << ft->at(2) << endl;
	cout << ft->at(3) << endl;
	delete ft;

	CFileIni *ini = new CFileIni("./test.ini");
	ini->write("pos/x", 100);
	ini->write("pos/y", 150);
	double dbl = ini->init("dbl", 3.25);
	cout << ini->read<int>("pos/x") << endl;
	cout << ini->read<int>("pos/y") << endl;
	cout << dbl << " == " <<ini->read<double>("dbl") << endl;
	auto array = ini->array("list/array");
	for (auto & value : array)
		cout << "read arr without defValue: " << value << endl;
	array = ini->array("list2/array", {"asd", "dsa", "13.7"});
	for (auto & value : array)
		cout << "read arr with defValue: " << value << endl;
	delete ini;

	CFileInfo *fi = new CFileInfo("./test.file.txt");
	cout << fi->file() << endl;
	cout << fi->path() << endl;
	cout << fi->fullName() << endl;
	cout << fi->name() << endl;
	cout << fi->suffix() << endl;
	cout << fi->ext() << endl;
	delete fi;


	auto st = new SerializationTest("./Serialization.ini");
	st->integer();
	st->floating();
	st->section__string();
	delete st;


	return 0;
}
