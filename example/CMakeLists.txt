cmake_minimum_required(VERSION 2.8)

project(FileMan_example)

find_program(CCACHE_FOUND ccache)
if(CCACHE_FOUND)
	set_property(GLOBAL PROPERTY RULE_LAUNCH_COMPILE ccache)
	set_property(GLOBAL PROPERTY RULE_LAUNCH_LINK ccache)
endif(CCACHE_FOUND)

aux_source_directory(${CMAKE_CURRENT_SOURCE_DIR} ${PROJECT_NAME}_LIST)

add_executable(${PROJECT_NAME} ${${PROJECT_NAME}_LIST})

target_link_libraries(${PROJECT_NAME} FileMan Serialization)
